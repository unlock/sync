#!/bin/bash
#============================================================================
#title          :Synchronisation of binaries and databases to compute nodes
#description    :Script to sync all needed resources from iRODS
#author         :Jasper Koehorst & Bart Nijsse
#date           :2021
#version        :0.0.2
#============================================================================
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

usage() { echo "Usage: $0 [-c|--cwl] [-r|--references] [-i|--infrastructure] [-s|--setup] [-a|--all]" 1>&2; exit 1; }

vars=$(getopt -o c:r:i:s:a: --long cwl:,references:,infrastructure:,setup:,all:)

#============================================================================
# KUBERNETES
#============================================================================

# Clean up finished sync pods
kubectl get pods --namespace='unlock' | grep munlock-sync | grep Completed | awk '{print "kubectl -n unlock delete pod "$1}' | sh
# Get all possible nodes
kubectl get nodes -l node-role.kubernetes.io/worker=worker | grep -v "SchedulingDisabled" | awk '{print $1}' | grep -v NAME > $DIR/nodes.txt

runall=false
if [ "$1" == "" ]; then
    runall=true
fi
for opt; do     
  if [[ $opt == "-a" || $opt == "--all" ]]; then
    runall=true
  fi
done
if $runall; then

  while read node; do
    sed "s/HOSTNAME/$node/g" $DIR/template_sync_cwl.yaml > $DIR/$node\_cwl.yaml
    sed "s/HOSTNAME/$node/g" $DIR/template_sync_infrastructure.yaml > $DIR/$node\_infrastructure.yaml
    sed "s/HOSTNAME/$node/g" $DIR/template_sync_references.yaml > $DIR/$node\_references.yaml
    sed "s/HOSTNAME/$node/g" $DIR/template_sync_setup.yaml > $DIR/$node\_setup.yaml
    kubectl apply -f $DIR/$node\_cwl.yaml
    kubectl apply -f $DIR/$node\_infrastructure.yaml
    kubectl apply -f $DIR/$node\_references.yaml
    kubectl apply -f $DIR/$node\_setup.yaml
   
    # Cleanup
    rm $DIR/$node\_cwl.yaml $DIR/$node\_infrastructure.yaml $DIR/$node\_references.yaml $DIR/$node\_setup.yaml

  done < $DIR/nodes.txt

  exit 1
fi

# If not all sent job for any given argument
for opt; do
  case "$opt" in
    -c|--cwl)

      while read node; do
        sed "s/HOSTNAME/$node/g" $DIR/template_sync_cwl.yaml > $DIR/$node\_cwl.yaml
        kubectl apply -f $DIR/$node\_cwl.yaml
        # Cleanup
        rm $DIR/$node\_cwl.yaml
      done < $DIR/nodes.txt

      ;;      
    -r|--references)

      while read node; do
        sed "s/HOSTNAME/$node/g" $DIR/template_sync_references.yaml > $DIR/$node\_references.yaml
        kubectl apply -f $DIR/$node\_references.yaml
        # Cleanup
        rm $DIR/$node\_references.yaml
      done < $DIR/nodes.txt

      ;;      
    -i|--infrastructure)

      while read node; do
        sed "s/HOSTNAME/$node/g" $DIR/template_sync_infrastructure.yaml > $DIR/$node\_infrastructure.yaml
        kubectl apply -f $DIR/$node\_infrastructure.yaml             
        # Cleanup
        rm $DIR/$node\_infrastructure.yaml
      done < $DIR/nodes.txt

      ;;
          -s|--setup)

      while read node; do
        sed "s/HOSTNAME/$node/g" $DIR/template_sync_setup.yaml > $DIR/$node\_setup.yaml
        kubectl apply -f $DIR/$node\_setup.yaml             
        # Cleanup
        rm $DIR/$node\_setup.yaml
      done < $DIR/nodes.txt

      ;;
    *)
      usage
  esac
done